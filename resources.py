import logging
import requests
import os
import random
from constants import *
import json
import numpy

from microraiden.proxy.resources import Expensive
from flask_restful import Resource
from flask import request, jsonify, make_response

log = logging.getLogger(__name__)

#TODO LOGS

class MonthlyAverageTemperature(Expensive):
    _price = 1

    def get(self, url, country, period_id, ensemble):
        with open('data/mavg_tas_AUS_1980-1999') as f:
            data = f.read()
            return jsonify(data)
        # period = PERIODS[period_id]
        # period_string = '{start}/{end}'.format(start=period['start'],
        #                                        end=period['end'])
        # if ensemble == 'true':
        #     return call_api_ensemble_request(Type.MAVG, Var.TAS, period_string, country)
        # else:
        #     return call_api_basic_request(Type.MAVG, Var.TAS, period_string, country)

class AnnualAverageTemperature(Expensive):
    _price = 3

    def get(self, url, country, period_id, ensemble):
        period = PERIODS[period_id]
        period_string = '{start}/{end}'.format(start=period['start'],
                                               end=period['end'])
        if ensemble == 'true':
            return call_api_ensemble_request(Type.ANNUALAVG, Var.TAS, period_string, country)
        else:
            return call_api_basic_request(Type.ANNUALAVG, Var.TAS, period_string, country)


class MonthlyAveragePrecipitation(Expensive):
    _price = 3

    def get(self, url, country, period_id, ensemble):
        period = PERIODS[period_id]
        period_string = '{start}/{end}'.format(start=period['start'],
                                               end=period['end'])
        if ensemble == 'true':
            return call_api_ensemble_request(Type.MAVG, Var.PR, period_string, country)
        else:
            return call_api_basic_request(Type.MAVG, Var.PR, period_string, country)

class AnnualAveragePrecipitation(Expensive):
    _price = 5

    def get(self, url, country, period_id, ensemble):
        period = PERIODS[period_id]
        period_string = '{start}/{end}'.format(start=period['start'],
                                               end=period['end'])
        if ensemble == 'true':
            return call_api_ensemble_request(Type.ANNUALAVG, Var.TAS, period_string, country)
        else:
            return call_api_basic_request(Type.ANNUALAVG, Var.PR, period_string, country)


def call_api_basic_request(type: Type, var: Var, period: str, country: str):
    log.info('Resource requested: {}'.format(request.url))

    request_url = '{domain}/{type}/{var}/{period}/{country}'.format(domain=CLIMATE_API_URL,
                                                                    type=type,
                                                                    var=var,
                                                                    period=period,
                                                                    country=country)
    log.info('Request url: {}'.format(request_url))
    data = requests.get(request_url).json()
    return jsonify(data)


def call_api_ensemble_request(type: Type, var: Var, period: str, country: str):
    log.info('Resource requested: {}'.format(request.url))

    request_url = '{domain}/{type}/ensemble/{var}/{period}/{country}'.format(domain=CLIMATE_API_URL,
                                                                    type=type,
                                                                    var=var,
                                                                    period=period,
                                                                    country=country)
    data = requests.get(request_url).json()
    return jsonify(data)


class DefaultPercentileData(Expensive):
    _price = 3

    def post(self, url):
        json_data = json.loads(request.get_json())
        all_month_vals = extract_json_data(json_data)
        percentiles = [10, 25, 50, 75, 90]
        response = []

        for percentile in percentiles:
            percentile_obj = { 'variable': json_data[0]['variable'],
                               'monthMedianVals': [],
                               'percentile': percentile,
                               'period': '{} - {}'.format(json_data[0]['fromYear'], json_data[0]['toYear'])
                               }

            for month in all_month_vals:
                percentile_array = numpy.array(month)
                p = numpy.percentile(percentile_array, percentile)
                percentile_obj['monthMedianVals'].append(p)

            response.append(percentile_obj)

        return jsonify(response)


class CustomPercentileData(Expensive):
    _price = 5

    def post(self, url, percentile):
        json_data = json.loads(request.get_json())
        all_month_vals = extract_json_data(json_data)

        response = { 'variable': json_data[0]['variable'],
                     'monthVals': [],
                     'percentile': percentile,
                     'fromYear': json_data[0]['fromYear'],
                     'toYear': json_data[0]['toYear'] }

        for month in all_month_vals:
            percentile_array = numpy.array(month)
            p = numpy.percentile(percentile_array, percentile)
            response['monthVals'].append(p)

        return jsonify(response)

def extract_json_data(json_data):
    all_month_vals = [[] for i in range(12)]  # List of each month's values
    for gcm in json_data:
        month_values = gcm['monthVals']

        # For each value in the current set of values, append to the corresponding month's list
        for i in range(12):
            all_month_vals[i].append(month_values[i])

    return all_month_vals

# ============================
# ||                        ||
# ||         STORAGE        ||
# ||                        ||
# ============================

class StoreFile(Expensive):
    _price = 2 # Underscore needed to prevent conflict with price() method in Resource

    def post(self, url, file_name):
        data = request.data # File data in binary format

        # Upload file to bucket
        try:
            url = '{}/upload/{}'.format(S3_SERVER, file_name)
            requests.post(url, data=data)
        except Exception as e:
            log.error(e)
            return ('Failed to upload file to test bucket.', 500)

        log.info('Successfully uploaded file to test bucket.')
        return ('Successfully uploaded file to test bucket.', 200)


class GetFile(Resource):
    def get(self, file_name):
        # Check local cache for file
        local_file = '{}/{}'.format(LOCAL_CACHE_PATH, file_name)

        if os.path.isfile(local_file):
            log.info('File {} found in local cache.'.format(file_name))

            with open(local_file, 'rb') as file:
                data = file.read()
                response = make_response(data)
                return response

        cache = [file for file in os.listdir(LOCAL_CACHE_PATH)]
        cache_size = len(cache)

        # If cache is full, randomly remove a file
        if cache_size >= MAX_CACHE_LIMIT:
            random_index = random.randint(0, cache_size - 1)
            file_to_remove = '{}/{}'.format(LOCAL_CACHE_PATH, cache[random_index])
            log.info('Local cache full. Removing file {}...'.format(cache[random_index]))
            os.remove(file_to_remove)

        # Retrieve file contents from test and write to local cache
        url = '{}/get/{}'.format(S3_SERVER, file_name)
        r = requests.get(url)
        log.info('Successfully retrieved file {} from S3'.format(file_name))

        response = make_response(r.content)
        return response