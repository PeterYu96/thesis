import click
import logging
from server import Server
from constants import DATA_PROVIDER_PORT
from resources import MonthlyAverageTemperature, \
    MonthlyAveragePrecipitation, \
    AnnualAverageTemperature, \
    AnnualAveragePrecipitation

from microraiden.proxy import PaywalledProxy
from microraiden.utils import get_private_key

log = logging.getLogger(__name__)


@click.command()
@click.option(
    '--private-key',
    required=True,
    help='The server\'s private key path.',
    type=str
)
def main(private_key: str):
    private_key = get_private_key(private_key)

    data_provider = Server(private_key)
    add_resources(data_provider.app)
    data_provider.run(port=DATA_PROVIDER_PORT)


def add_resources(app: PaywalledProxy):
    app.add_paywalled_resource(MonthlyAverageTemperature,
                               '/mavgtemp/<string:country>/<string:period_id>/<string:ensemble>',
                               price=MonthlyAverageTemperature.price)
    app.add_paywalled_resource(MonthlyAveragePrecipitation,
                               '/mavgpr/<string:country>/<string:period_id>/<string:ensemble>',
                               price=MonthlyAveragePrecipitation.price)
    app.add_paywalled_resource(AnnualAverageTemperature,
                               '/annualavgtemp/<string:country>/<string:period_id>/<string:ensemble>',
                               price=AnnualAverageTemperature.price)
    app.add_paywalled_resource(AnnualAveragePrecipitation,
                               '/annualavgpr/<string:country>/<string:period_id>/<string:ensemble>',
                               price=AnnualAveragePrecipitation.price)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
