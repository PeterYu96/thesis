from constants import *
from termcolor import cprint

# ====================
# ||                ||
# ||    MAIN MENU   ||
# ||                ||
# ====================

def print_menu_banner():
    cprint('\n'
           '=============================================\n'
           '*                                           *\n'
           '*            MICROPAYMENT CLIENT            *\n'
           '*                                           *\n'
           '=============================================\n',
           color='blue')

def print_menu_commands():
    cprint('\n'
           'COMMANDS\n'
           '\n'
           'data:          Open a payment channel with a data provider and get a requested resource\n'
           'analytics:     Get analytics data\n'
           'storage:       Open a payment channel with a storage provider\n'
           'quit:          Close all client sessions and their channels (if open)\n',
           color='blue')

# ====================
# ||                ||
# ||    DATA MENU   ||
# ||                ||
# ====================

def print_data_banner():
    cprint('=============================================\n'
           '*                                           *\n'
           '*                DATA PROVIDER              *\n'
           '*                                           *\n'
           '=============================================',
           color='yellow')

def print_resources():
    cprint('\n'
           'AVAILABLE RESOURCES\n'
           '\n'
           'mavgtemp:           Monthly average temperature\n\n'
           'mavgpr:             Monthly average precipitation\n\n'
           'annualavgtemp:      Annual average temperature\n\n'
           'annualavgpr:        Annual average precipitation\n',
           color='yellow')

def print_periods():
    cprint('PERIODS\n', color='yellow')
    for index, period in PERIODS.items():
        if index == '1':
            cprint('\nPAST', color='yellow')
        elif index == '5':
            cprint('\nFUTURE', color='yellow')

        cprint('({list_index}) {start} to {end}'.format(list_index=index,
                                                        start=period['start'],
                                                        end=period['end']),
               color='yellow')

def print_countries():
    for code, country in COUNTRIES.items():
        cprint('{country} - {code}'.format(country=country, code=code), color='yellow')


# ============================
# ||                        ||
# ||     ANALYTICS MENU     ||
# ||                        ||
# ============================

def print_analytics_banner():
    cprint('\n'
           '=============================================\n'
           '*                                           *\n'
           '*             ANALYTICS PROVIDER            *\n'
           '*                                           *\n'
           '=============================================',
           color='yellow')


def print_analytics_commands():
    cprint('\nCOMMANDS:\n'
           '\n'
           'default:       Get the 10th, 25th, 50th, 75th and 90th percentiles of a dataset\n'
           'custom:        Get a custom percentile of a dataset\n'
           'back:          Return to main menu\n',
           color='yellow')


# ============================
# ||                        ||
# ||      STORAGE MENU      ||
# ||                        ||
# ============================

def print_storage_banner():
    cprint('=============================================\n'
           '*                                           *\n'
           '*              STORAGE PROVIDER             *\n'
           '*                                           *\n'
           '=============================================',
           color='yellow')

def print_storage_commands():
    cprint('\nCOMMANDS:\n'
           '\n'
           'store:      Store a file\n'
           'get:        Get a file from storage\n'
           'back:       Return to main menu\n',
           color='yellow')