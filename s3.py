# HELPER SERVER FOR INTERACTING WITH S3
# Required to keep microraiden and boto3 separate

import boto3
import botocore

S3_BUCKET_NAME = 'micropayment-storage-provider'
s3 = boto3.resource('s3')
bucket = s3.Bucket(S3_BUCKET_NAME)

from flask import make_response, Flask, request
app = Flask('s3')

@app.route('/get/<file_name>')
def download_file(file_name):
    # File in local cache to write to
    local_file = './storage/localcache/{}'.format(file_name)

    # Retrieve file contents from S3 bucket, write to local cache and return to client
    with open(local_file, 'ab+') as f:
        try:
            bucket.download_fileobj(file_name, f)

            f.flush()
            f.seek(0)  # Return to start of file for reading
            data = f.read()

        except botocore.errorfactory.ClientError as e:

            if e.response['Error']['Code'] == '404':
                return ('File {} does not exist in storage'.format(file_name), 404)
            else:
                return ('Failed to get file storage.', 500)

    response = make_response(data)
    return response

@app.route('/upload/<file_name>', methods = ['POST'])
def upload_file(file_name):
    bucket.put_object(Body=request.data, Key=file_name)
