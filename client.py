import timeit
import click
import os
from web3 import Web3
from constants import *
from helper import *
from termcolor import cprint

from microraiden import Session
from constants import CHANNEL_MANAGER_ADDRESS

import logging
import requests

@click.command()
@click.option(
    '--private-key',
    required=True,
    help='Path to private key file or a hex-encoded private key.',
    type=click.Path(exists=True, dir_okay=False, resolve_path=True)
)
@click.option(
    '--password-path',
    default=None,
    help='Path to file containing the password for the private key specified.',
    type=click.Path(exists=True, dir_okay=False, resolve_path=True)
)
def main(
        private_key: str,
        password_path: str
):
    run(private_key, password_path)

def run(
        private_key: str,
        password_path: str,
        channel_manager_address: str = CHANNEL_MANAGER_ADDRESS,
        web3: Web3 = None,
        retry_interval: float = 5,
        data_endpoint_url: str = 'http://localhost:{}'.format(DATA_PROVIDER_PORT),
        analytics_endpoint_url = 'http://localhost:{}'.format(ANALYTICS_PROVIDER_PORT),
        storage_endpoint_url: str = 'http://localhost:{}'.format(STORAGE_PROVIDER_PORT)
):
    # Create the client session.
    # data_session = Session(
    #     endpoint_url=data_endpoint_url,
    #     private_key=private_key,
    #     key_password_path=password_path,
    #     channel_manager_address=channel_manager_address,
    #     web3=web3,
    #     retry_interval=retry_interval
    # )

    analytics_session = Session(
        endpoint_url=analytics_endpoint_url,
        private_key=private_key,
        key_password_path=password_path,
        channel_manager_address=channel_manager_address,
        web3=web3,
        retry_interval=retry_interval
    )
    # storage_session = Session(
    #     endpoint_url=storage_endpoint_url,
    #     private_key=private_key,
    #     key_password_path=password_path,
    #     channel_manager_address=channel_manager_address,
    #     web3=web3,
    #     retry_interval=retry_interval
    # )


    print_menu_banner()
    user_input = ''
    while user_input != 'quit' and user_input != 'q':
        user_input = input('Enter a command [? for menu]: ')

        if user_input == '?':
            print_menu_commands()

        elif user_input == 'data':
            data_menu(data_session)
            print_menu_banner()

        elif user_input == 'analytics':
            analytics_menu(analytics_session, analytics_endpoint_url)
            print_menu_banner()

        elif user_input == 'storage':
            # storage_menu(storage_session, storage_endpoint_url)
            print_menu_banner()

    # data_session.close()
    analytics_session.close()
    #storage_session.close()

def data_menu(session: Session):
    print_resources()

    # Get the resource. If payment is required, client will attempt to create
    # a channel or will use existing one.
    resource = ''
    while resource != 'back':
        resource = input('Enter a resource: ')

        # Validate user entered a valid endpoint
        if resource not in DATA_ENDPOINTS:
            cprint('{resource} is not a valid resource.\n'.format(resource=resource), color='red')
            continue
        else:
            country = ''
            while country != 'back':
                country = input('Enter a 3-letter country code [? for country codes]: ')

                if country == '?':
                    print_countries()
                    continue

                if country not in COUNTRIES:
                    cprint('{country} is not a valid country code.\n'.format(country=country), color='red')
                    continue

                # List out available periods to select from
                print_periods()

                period_id = input('Select a period ID: ')

                # Instead of returning data for each GCM model, user can also get the results as an ensemble,
                # getting the 10th, 50th (median) and 90th percentile values of all models together
                get_percentiles = input('Return percentiles? [y/n]: ')
                ensemble = 'true' if get_percentiles == 'y' else 'false'
                get_resource(session, session.endpoint_url, resource, country,
                             period_id, ensemble)
                break



def get_resource(session: Session, endpoint_url: str, resource: str, country: str, period_id: str, ensemble: str):
    numTransactions = 100
    csv_line = ''
    for i in range(1, numTransactions+1):

        start_time = timeit.default_timer() # Measuring transaction times for results

        # Send a request to the server for the resource
        # TODO REMOVE AFTER - FOR TESTING

        response = session.get('{url}/{resource}/{country}/{period_id}/{ensemble}'.format(url=endpoint_url,
                                                                                          resource=resource,
                                                                                          country=country,
                                                                                          period_id=period_id,
                                                                                          ensemble=ensemble))
        elapsed_time = (timeit.default_timer() - start_time) * 1000
        elapsed_time = round(elapsed_time, 3)

        print('Time elapsed for transaction {transaction_id} is: {time} milliseconds'.format(transaction_id=i, time=elapsed_time))
        csv_line += '{time}, '.format(time=elapsed_time)

    with open('time_results.csv', 'a') as results:
        results.write(csv_line + '\n')
    # return response

def analytics_menu(session: Session, endpoint_url: str):
    # Send a request to the server for the resource
    command = ''
    print_analytics_banner()

    while command != 'back':

        file_name = input('Enter a file name: ')
        with open('./data/{}'.format(file_name), 'r') as f:
            json = f.read()

        print_analytics_commands()

        command = input('Enter a command: ')
        if command == 'default':

            response = session.post('{}/default'.format(endpoint_url), json=json)

            handle_response(response, 'Default Percentile Values')

        elif command == 'custom':
            percentile = input('Enter a percentile between 0 and 100 (inclusive): ')
            response = session.post('{}/custom/{}'.format(endpoint_url, percentile), json=json)

            handle_response(response, '{}th Percentile Values'.format(percentile))



def storage_menu(session: Session, endpoint_url: str):
    # Get the resource. If payment is required, client will attempt to create
    # a channel or will use existing one.
    command = ''

    print_storage_banner()

    while command != 'back':
        print_storage_commands()

        command = input('Enter a command: ')
        response = ''
        if command == 'store':
            file_name = input('Enter the name of the file to store: ')
            file_path = '{}/{}'.format(LOCAL_CACHE_PATH, file_name)

            if not os.path.isfile(file_path):
                print('{} does not exist'.format(file_name))
            else:
                with open(file_path, 'rb') as file: # Open the file in binary mode to send as data in the request
                    data = file.read()
                    response = session.post('{}/store/{}'.format(endpoint_url, file_name), data=data)
                    handle_response(response, 'Store File')


        elif command == 'get':
            # Check if file exists in test bucket, if so, retrieve
            file_name = input('Enter name of file to retrieve: ')
            response = session.get('{}/get/{}'.format(endpoint_url, file_name))

            file_path = './{}'.format(file_name)
            with open(file_path, 'wb') as file:
                file.write(response.content)

            handle_response(response, '')



def handle_response(response, resource):
    if response.status_code == requests.codes.OK:
        logging.info(
            "Got the resource {} type={}:\n{}".format(
                resource,
                response.headers.get('Content-Type', '???'),
                response.text
            )
        )
    else:
        logging.error(
            "Error getting the resource. Code={} body={}".format(
                response.status_code,
                response.text
            )
        )

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()