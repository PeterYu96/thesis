import click
import logging
from resources import StoreFile, GetFile # NOTE: must include beforehand

from server import Server
from constants import STORAGE_PROVIDER_PORT
from microraiden.proxy import PaywalledProxy
from microraiden.utils import get_private_key

log = logging.getLogger(__name__)

@click.command()
@click.option(
    '--private-key',
    required=True,
    help='The server\'s private key path.',
    type=str
)
def main(private_key: str):
    private_key = get_private_key(private_key)

    storage_provider = Server(private_key)
    add_resources(storage_provider.app)
    storage_provider.run(port=STORAGE_PROVIDER_PORT)


def add_resources(app: PaywalledProxy):
    app.add_paywalled_resource(StoreFile, '/store/<string:file_name>', price=StoreFile._price)
    app.api.add_resource(GetFile, '/get/<string:file_name>')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
