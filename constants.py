from enum import Enum

DATA_PROVIDER_PORT = 5001
ANALYTICS_PROVIDER_PORT = 5002
STORAGE_PROVIDER_PORT = 5003

CHANNEL_MANAGER_ADDRESS = '0xf0ac7383a2067450123b33d1d0b0db7ac8468819'

CLIMATE_API_URL = 'http://climatedataapi.worldbank.org/climateweb/rest/v1/country'

DATA_ENDPOINTS = {'mavgtemp', 'mavgpr', 'annualavgtemp', 'annualavgpr'}

S3_BUCKET_NAME = 'micropayment-storage-provider'
S3_SERVER = 'http://localhost:5000'
LOCAL_CACHE_PATH = './storage/localcache'
MAX_CACHE_LIMIT = 10

class CustEnum(Enum):
    # Overwrite Enum string function to just return value
    def __str__(self):
        return '%s' % self._value_

# Anomaly types are only relevant to future periods. Using these parameters for time periods before
# 2000 will return empty responses.
class Type(CustEnum):
    MAVG = 'mavg' # Monthly average
    ANNUALAVG = 'annualavg' # Annual average
    MANOM = 'manom' # Average monthly change (anomaly)
    ANNUALANOM = 'annualanom' # Average annual change (anomaly)


class Var(CustEnum):
    PR = 'pr' # Precipitation (rainfall and assumed water equivalent), in millimeters
    TAS = 'tas' # Temperature, in degrees Celsius


# Years must be paired as so for API calls
PERIODS = {
    '1': {'name': 'NINETEEN_TWENTY', 'start': '1920', 'end': '1939'},
    '2': {'name': 'NINETEEN_FORTY', 'start': '1940', 'end': '1959'},
    '3': {'name': 'NINETEEN_SIXTY', 'start': '1960', 'end': '1979'},
    '4': {'name': 'NINETEEN_EIGHTY', 'start': '1980', 'end': '1999'},
    '5': {'name': 'TWENTY_TWENTY', 'start': '2020', 'end': '2039'},
    '6': {'name': 'TWENTY_FORTY', 'start': '2040', 'end': '2059'},
    '7': {'name': 'TWENTY_SIXTY', 'start': '2060', 'end': '2079'},
    '8': {'name': 'TWENTY_EIGHTY', 'start': '2080', 'end': '2099'}
}

# ISO-alpha3 country codes
COUNTRIES = {
    'ARG' : 'Argentina',
    'AUS' : 'Australia',
    'BRA' : 'Brazil',
    'CAN' : 'Canada',
    'CHN' : 'China',
    'EGY' : 'Egypt',
    'FRA' : 'France',
    'GBR' : 'Great Britain',
    'DEU' : 'Germany',
    'IND' : 'India',
    'IRQ' : 'Iraq',
    'MEX' : 'Mexico',
    'RUS' : 'Russia',
    'ZAF' : 'South Africa',
    'SWE' : 'Sweden',
    'USA' : 'United States of America'
}



class Stat(CustEnum):
    f = 3