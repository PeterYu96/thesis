import os
import click
import logging
from web3 import Web3, HTTPProvider

from microraiden.channel_manager import ChannelManager
from microraiden.make_helpers import make_channel_manager
from microraiden.constants import WEB3_PROVIDER_DEFAULT
from microraiden.config import NETWORK_CFG
from microraiden.proxy import PaywalledProxy

log = logging.getLogger(__name__)

class Server():
    def __init__(
            self,
            private_key: str,
            state_file_path: str = os.path.join(click.get_app_dir('microraiden'), 'echo_server.db'),
            channel_manager: ChannelManager = None,
            join_thread: bool = True
    ) -> None:
        dirname = os.path.dirname(state_file_path) #TODO Need separate state file paths for providers
        if dirname:
            os.makedirs(dirname, exist_ok=True)

        # Set up paywalled proxy for accessing a service
        # arguments are:
        #  - private key to use for receiving funds
        #  - file for storing state information (balance proofs)
        if channel_manager is None:
            web3 = Web3(HTTPProvider(WEB3_PROVIDER_DEFAULT))
            NETWORK_CFG.set_defaults(int(web3.version.network))
            channel_manager = make_channel_manager(
                private_key,
                NETWORK_CFG.CHANNEL_MANAGER_ADDRESS,
                state_file_path,
                web3
            )
        self.app = PaywalledProxy(channel_manager)
        self.join_thread = join_thread

    def run(self, port: int):
        # coloredlogs.install()

        # Start the app. proxy is a WSGI greenlet, so must join it properly
        self.app.run(debug=True, port=port)

        if self.join_thread:
            self.app.join()
        else:
            return self.app