import requests
from flask import jsonify

# Years must be paired as so for API calls
PERIODS = {
    '1': {'name': 'NINETEEN_TWENTY', 'start': '1920', 'end': '1939'},
    '2': {'name': 'NINETEEN_FORTY', 'start': '1940', 'end': '1959'},
    '3': {'name': 'NINETEEN_SIXTY', 'start': '1960', 'end': '1979'},
    '4': {'name': 'NINETEEN_EIGHTY', 'start': '1980', 'end': '1999'},
    '5': {'name': 'TWENTY_TWENTY', 'start': '2020', 'end': '2039'},
    '6': {'name': 'TWENTY_FORTY', 'start': '2040', 'end': '2059'},
    '7': {'name': 'TWENTY_SIXTY', 'start': '2060', 'end': '2079'},
    '8': {'name': 'TWENTY_EIGHTY', 'start': '2080', 'end': '2099'}
}

# ISO-alpha3 country codes
COUNTRIES = {
    'ARG' : 'Argentina',
    'AUS' : 'Australia',
    'BRA' : 'Brazil',
    'CAN' : 'Canada',
    'CHN' : 'China',
    'EGY' : 'Egypt',
    'FRA' : 'France',
    'GBR' : 'Great Britain',
    'DEU' : 'Germany',
    'IND' : 'India',
    'IRQ' : 'Iraq',
    'MEX' : 'Mexico',
    'RUS' : 'Russia',
    'ZAF' : 'South Africa',
    'SWE' : 'Sweden',
    'USA' : 'United States of America'
}

CLIMATE_API_URL = 'http://climatedataapi.worldbank.org/climateweb/rest/v1/country'

DATA_ENDPOINTS = {'mavg', 'annualavg'}

var = 'pr'

request_url = '{domain}/mavg/tas/1980/1999/AUS'.format(domain=CLIMATE_API_URL)
data = requests.get(request_url).json()
file_name = '{}_{}_{}_{}-{}'.format('mavg', 'tas', 'AUS', '1980', '1999')
with open(file_name, 'x') as file:
    file.write(str(data))

# for type in DATA_ENDPOINTS:
#     for id, p in PERIODS.items():
#         start = p['start']
#         end = p['end']
#         period = '{}/{}'.format(start, end)
#
#         for country in COUNTRIES:
#             request_url = '{domain}/{type}/{var}/{period}/{country}'.format(domain=CLIMATE_API_URL,
#                                                                             type=type,
#                                                                             var=var,
#                                                                             period=period,
#                                                                             country=country)
#
#             data = requests.get(request_url).json()
#             print(data)
#
#             file_name = '{}_{}_{}_{}-{}'.format(type, var, country, start, end)
#             with open(file_name, 'x') as file:
#                 file.write(str(data))