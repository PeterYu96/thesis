import click
import logging
from server import Server
from constants import ANALYTICS_PROVIDER_PORT
from resources import DefaultPercentileData, CustomPercentileData

from microraiden.proxy import PaywalledProxy
from microraiden.utils import get_private_key

log = logging.getLogger(__name__)


@click.command()
@click.option(
    '--private-key',
    required=True,
    help='The server\'s private key path.',
    type=str
)
def main(private_key: str):
    private_key = get_private_key(private_key)

    analytics_provider = Server(private_key)
    add_resources(analytics_provider.app)
    analytics_provider.run(port=ANALYTICS_PROVIDER_PORT)


def add_resources(app: PaywalledProxy):
    app.add_paywalled_resource(DefaultPercentileData, '/default', price=DefaultPercentileData._price)
    app.add_paywalled_resource(CustomPercentileData, '/custom/<int:percentile>', price=CustomPercentileData._price)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
